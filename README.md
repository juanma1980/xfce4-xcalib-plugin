[![License](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://gitlab.com/juanma1980/xfce4-xcalib-plugin/-/blob/master/COPYING)

# xfce4-xcalib-plugin

Xfce4-xcalib-plugin is a simple xcalib wrapper for the Xfce panel 


----

### Homepage

[Xfce4-xcalib-plugin documentation](https://gitlab.com/juanma1980/xfce4-xcalib-plugin)

### Changelog

See [NEWS](https://gitlab.com/juanma1980/xfce4-xcalib-plugin/-/blob/master/NEWS) for details on changes and fixes made in the current release.

### Source Code Repository

[Xfce4-xcalib-plugin source code](https://gitlab.com/juanma1980/xfce4-xcalib-plugin)

### Installation

From source code repository: 

    % cd xfce4-xcalib-plugin
    % ./autogen.sh
    % make
    % make install

### Reporting Bugs

Visit the [reporting bugs](https://gitlab.com/juanma1980/xfce4-xcalib-plugin/bugs) page to view currently open bug reports and instructions on reporting new bugs or submitting bugfixes.

