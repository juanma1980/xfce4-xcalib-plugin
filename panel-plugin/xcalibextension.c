/*  $Id$
 *  Copyright (C) 2024 juanma1980 <juanma1980@disroot.org>
 *  Simple extension for inverting screen color through xcalib
 *  Based on xcalibextension plugin and licensed under same terms.
 *  
 *  Copyright (C) 2019 John Doo <john@foo.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include <libxfce4util/libxfce4util.h>
#include <libxfce4panel/libxfce4panel.h>

#include "xcalibextension.h"
#include "xcalibextension-dialogs.h"

/* default settings */
#define OPERATION_MODE FALSE


/* prototypes */
static void xcalibextension_construct (XfcePanelPlugin *plugin);


void toggleInvert(void)
{
    int status;
    char *args[4];
    DBG("Calling xcalib");
    args[0] = "/usr/bin/xcalib";        // first arg is the full path to the executable
    args[1] = "-i";             // list of args must be NULL terminated
    args[2] = "-a";             // list of args must be NULL terminated
    args[3] = NULL;             // list of args must be NULL terminated

    if ( fork() == 0 )
	{
		DBG("EXEC FORK");
		if (execv(args[0],  args) ==  -1)
                       printf("Error '%s':%s\n",args[0],  strerror(errno));
    } else {
        wait( &status );
	}
}


void xcalibextension_save (XfcePanelPlugin *plugin,
             xcalibextensionPlugin    *xcalibextension)
{
  XfceRc *rc;
  gchar  *file;

  /* get the config file location */
  file = xfce_panel_plugin_save_location (plugin, TRUE);

  if (G_UNLIKELY (file == NULL))
    {
       DBG ("Failed to open config file");
       return;
    }

  /* open the config file, read/write */
  rc = xfce_rc_simple_open (file, FALSE);
  g_free (file);

  if (G_LIKELY (rc != NULL))
    {
      /* save the settings */
      DBG(".");
	  DBG("SAVE: %d\n",xcalibextension->mode);
      xfce_rc_write_bool_entry (rc, "mode", xcalibextension->mode);

      /* close the rc file */
      xfce_rc_close (rc);
    }
}

static void xcalibextension_read (xcalibextensionPlugin *xcalibextension)
{
  XfceRc      *rc;
  gchar       *file;

  /* get the plugin config file location */
  if((file = xfce_panel_plugin_lookup_rc_file(xcalibextension->plugin)) != NULL)
    {
      /* open the config file, readonly */
      rc = xfce_rc_simple_open (file, TRUE);

      /* cleanup */
      g_free (file);

      if (rc != NULL)
        {
            DBG("READ FILE");
          /* read the settings */
          xcalibextension->mode = xfce_rc_read_bool_entry (rc, "mode", xcalibextension->mode);

          /* cleanup */
          xfce_rc_close (rc);

          /* leave the function, everything went well */
          return;
        }
    }

  /* something went wrong, apply default values */
  DBG ("Applying default settings");

  xcalibextension->mode = FALSE;
}



static xcalibextensionPlugin * xcalibextension_new (XfcePanelPlugin *plugin)
{
  xcalibextensionPlugin   *xcalibextension;
  GtkOrientation  orientation;
  GtkWidget *btn;
  GtkWidget *image = gtk_image_new_from_icon_name("xfce4-xcalib-plugin",GTK_ICON_SIZE_LARGE_TOOLBAR);

  /* allocate memory for the plugin structure */
  xcalibextension = g_slice_new0 (xcalibextensionPlugin);

  /* pointer to plugin */
  xcalibextension->plugin = plugin;

  /* read the user settings */
  xcalibextension_read (xcalibextension);

  /* get the current orientation */
  orientation = xfce_panel_plugin_get_orientation (plugin);

  /* create some panel widgets */
  xcalibextension->ebox = gtk_event_box_new ();
  gtk_event_box_set_visible_window (GTK_EVENT_BOX (xcalibextension->ebox), FALSE);
  gtk_widget_show (xcalibextension->ebox);

  xcalibextension->hvbox = gtk_box_new (orientation, 2);
  gtk_widget_show (xcalibextension->hvbox);
  gtk_container_add (GTK_CONTAINER (xcalibextension->ebox), xcalibextension->hvbox);

  /* some xcalibextension widgets */
  btn = gtk_button_new_with_label("I");
  gtk_button_set_image(btn, image);

  gtk_widget_show (btn);
  g_signal_connect(G_OBJECT(btn), "clicked",
      G_CALLBACK(toggleInvert), NULL);
  gtk_box_pack_start (GTK_BOX (xcalibextension->hvbox), btn, FALSE, FALSE, 0);


  return xcalibextension;
}



static void
xcalibextension_free (XfcePanelPlugin *plugin,
             xcalibextensionPlugin    *xcalibextension)
{
  GtkWidget *dialog;

  /* check if the dialog is still open. if so, destroy it */
  dialog = g_object_get_data (G_OBJECT (plugin), "dialog");
  if (G_UNLIKELY (dialog != NULL))
    gtk_widget_destroy (dialog);

  /* destroy the panel widgets */
  gtk_widget_destroy (xcalibextension->hvbox);

  /* cleanup the settings */
 // if (G_LIKELY (xcalibextension->mode != NULL))
 //   g_free (xcalibextension->mode);

  /* free the plugin structure */
  g_slice_free (xcalibextensionPlugin, xcalibextension);
}



static void
xcalibextension_orientation_changed (XfcePanelPlugin *plugin,
                            GtkOrientation   orientation,
                            xcalibextensionPlugin    *xcalibextension)
{
  /* change the orientation of the box */
  gtk_orientable_set_orientation(GTK_ORIENTABLE(xcalibextension->hvbox), orientation);
}



static gboolean
xcalibextension_size_changed (XfcePanelPlugin *plugin,
                     gint             size,
                     xcalibextensionPlugin    *xcalibextension)
{
  GtkOrientation orientation;

  /* get the orientation of the plugin */
  orientation = xfce_panel_plugin_get_orientation (plugin);

  /* set the widget size */
  if (orientation == GTK_ORIENTATION_HORIZONTAL)
    gtk_widget_set_size_request (GTK_WIDGET (plugin), -1, size);
  else
    gtk_widget_set_size_request (GTK_WIDGET (plugin), size, -1);

  /* we handled the orientation */
  return TRUE;
}



static void
xcalibextension_construct (XfcePanelPlugin *plugin)
{
  xcalibextensionPlugin *xcalibextension;

  /* setup transation domain */
  xfce_textdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR, "UTF-8");

  /* create the plugin */
  xcalibextension = xcalibextension_new (plugin);

  /* add the ebox to the panel */
  xfce_panel_plugin_set_small(plugin, TRUE);
  gtk_container_add (GTK_CONTAINER (plugin), xcalibextension->ebox);

  /* show the panel's right-click menu on this ebox */

  /* connect plugin signals */
  g_signal_connect (G_OBJECT (plugin), "free-data",
                    G_CALLBACK (xcalibextension_free), xcalibextension);

  g_signal_connect (G_OBJECT (plugin), "save",
                    G_CALLBACK (xcalibextension_save), xcalibextension);

  g_signal_connect (G_OBJECT (plugin), "size-changed",
                    G_CALLBACK (xcalibextension_size_changed), xcalibextension);

  g_signal_connect (G_OBJECT (plugin), "orientation-changed",
                    G_CALLBACK (xcalibextension_orientation_changed), xcalibextension);

  /* show the configure menu item and connect signal */
  xfce_panel_plugin_menu_show_configure (plugin);
  g_signal_connect (G_OBJECT (plugin), "configure-plugin",
                    G_CALLBACK (xcalibextension_configure), xcalibextension);

  /* show the about menu item and connect signal */
  xfce_panel_plugin_menu_show_about (plugin);
  g_signal_connect (G_OBJECT (plugin), "about",
                    G_CALLBACK (xcalibextension_about), NULL);

  xfce_panel_plugin_add_action_widget (plugin, xcalibextension->ebox);

}

/* register the plugin */
XFCE_PANEL_PLUGIN_REGISTER (xcalibextension_construct);
